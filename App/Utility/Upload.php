<?php

namespace App\Utility;

class Upload {


    public static function uploadFile($file, $fileName)
    {
        $currentDirectory = getcwd();
        $uploadDirectory = "/storage/";


        $fileExtensionsAllowed = ['jpeg', 'jpg', 'png'];

        $fileSize = $file['size'];
        $fileTmpName = $file['tmp_name'];

        $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
        $pictureName = basename($fileName . '.'. $fileExtension);


        $uploadPath = $currentDirectory . $uploadDirectory . $pictureName;

        if (!in_array($fileExtension, $fileExtensionsAllowed)) {
            throw new \Exception("Cette extension de fichier n'est pas autorisé, merci d'utiliser un JPEG ou PNG");
        }

        if ($fileSize > 4000000) {
            throw new \Exception("Taille maximale excedée (4MB)");
        }

        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

        if ($didUpload) {
            return $pictureName;
        } else {
            throw new \Exception("Une erreur est survenue merci de contacter l'administrateur");
        }
    }
}
