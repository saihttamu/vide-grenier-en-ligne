FROM php:7.4-apache

#Install git and MySQL extensions for PHP
WORKDIR /var/www/html
RUN apt-get update && apt-get install -y git
RUN apt-get install -y zip unzip
RUN docker-php-ext-install pdo pdo_mysql mysqli
RUN apt-get install -y zip unzip
COPY ./Docker-vhost.conf /etc/apache2/sites-enabled/000-default.conf

# Installation de Composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# RUN composer update
# RUN composer dump-autoload -o

RUN a2enmod rewrite && service apache2 restart

COPY . /var/www/html/
RUN chmod -R 777 /var/www/html/public
RUN composer install
RUN composer update
RUN composer dump-autoload -o
EXPOSE 80/tcp
EXPOSE 443/tcp
